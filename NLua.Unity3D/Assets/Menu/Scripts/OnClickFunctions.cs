﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OnClickFunctions : MonoBehaviour
{

    public Text levelOneScore;
    public Text levelTwoScore;
    public Text levelThreeScore;
    public Text totalScore;
    public Text buttonTwoText;
    public Text buttonThreeText;
    void Start()
    {

    }
    public void loadLevelOnClick(int level)
    {
        if (level > 0)
        {
            if (level == 2 && GameManager.instance.LevelOneCompleted == false) return;
        }
        Application.LoadLevel(level);
    }

    public void exitOnClick()
    {
        Application.Quit();
    }

    void Update()
    {
        if (GameManager.instance.LevelOneCompleted == false)
        {
            buttonTwoText.text = "Locked";
        }
        else
        {
            buttonTwoText.text = "Level Two";
        }
        if (GameManager.instance.LevelTwoCompleted == false)
        {
            buttonThreeText.text = "Locked";
        }
        else
        {
            buttonThreeText.text = "Level Three";
        }
        levelOneScore.text = "Score: " + GameManager.instance.LevelOneScore;
        levelTwoScore.text = "Score: " + GameManager.instance.LevelTwoScore;
        levelThreeScore.text = "Score: " + GameManager.instance.LevelThreeScore;
        totalScore.text = "Total Score: " + GameManager.instance.TotalScore;

    }
}
