﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{


    int topLevelOneScore = 0;
    int topLevelTwoScore = 0;
    int topLevelThreeScore = 0;
    int bestScore = 0;
    int[] levelOneScores = new int[3];
    int[] levelTwoScores = new int[3];
    int[] levelThreeScores = new int[3];
    public GameObject[] robots;

    bool levelOneComplete = false;
    bool levelTwoComplete = false;

    public bool LevelOneCompleted
    {
        get { return levelOneComplete; }
    }
    public bool LevelTwoCompleted
    {
        get { return levelTwoComplete; }
    }
    public int LevelOneScore
    {
        get { return topLevelOneScore; }
    }
    public int LevelTwoScore
    {
        get { return topLevelTwoScore; }
    }
    public int LevelThreeScore
    {
        get { return topLevelThreeScore; }
    }
    public int TotalScore
    {
        get { return bestScore; }
    }

    public static GameManager instance;
    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {

            instance = this;
        }
        DontDestroyOnLoad(instance);
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void getRobotsOnLevelLoad()
    {
        robots = GameObject.FindGameObjectsWithTag("Robot");
    }

    public int getLevelScore()
    {
        int levelScore = 0;

        robots = GameObject.FindGameObjectsWithTag("Robot");

        foreach (GameObject i in robots)
        {
            Droid stats;
            if (i.GetComponent("Robot") == null && i.GetComponent("Door") == null)
            {
                continue;
            }
            else if (i.GetComponent("Robot") == null)
            {
                stats = i.GetComponent("Door") as Door;
                levelScore += stats.timesInteracted;
            }
            else
            {
                stats = i.GetComponent("Robot") as Robot;
                levelScore += stats.timesInteracted;
            }

        }

        return levelScore;
    }

    public void updateLevelScore(int level, int score)
    {

        switch (level)
        {
            case 1:
                {
                    updateHighScores(levelThreeScores, "levelOne ", score);
                    if (score < topLevelOneScore || topLevelOneScore == 0 )
                    {
                        topLevelOneScore = score;
                        bestScore = topLevelOneScore + topLevelTwoScore + topLevelThreeScore;
                    }
                    levelOneComplete = true;
                    break;
                }
            case 2:
                {
                    updateHighScores(levelThreeScores, "levelTwo ", score);
                    if (score < topLevelTwoScore || topLevelOneScore == 0)
                    {
                        topLevelTwoScore = score;
                        bestScore = topLevelOneScore + topLevelTwoScore + topLevelThreeScore;
                    }
                    levelTwoComplete = true;
                    break;
                }
            case 3:
                {
                    updateHighScores(levelThreeScores, "levelThree ", score);
                    if (score < topLevelThreeScore || topLevelOneScore == 0)
                    {
                        topLevelThreeScore = score;
                        bestScore = topLevelOneScore + topLevelTwoScore + topLevelThreeScore;
                    }
                    break;
                }
        }
    }
    private void updateHighScores(int[] level, string key, int score)
    {
        for (int i = 0; i < level.Length; i++)
        {

            string highScoreKey = key + (i + 1).ToString();
            int highScore = PlayerPrefs.GetInt(highScoreKey, 0);
            if (score < highScore)
            {
                int temp = highScore;
                PlayerPrefs.SetInt(highScoreKey, score);
                score = temp;
            }
        }
    }
}
