﻿using UnityEngine;
using System.Collections;

public class WorldControl : MonoBehaviour
{

    public static GameObject selected;

    public string tutorialPart = " ";

    public GameObject target;
    public GameObject door;
    public GameObject robot;

    int toolbarNumber;
    int togglebarNumber;
    public bool targetDestoryed = false;

    private Rect mainGUIRect = new Rect(Screen.width / Screen.width, Screen.height - 200, Screen.width, 200);
    private Rect timeGUIRect = new Rect(Screen.width / 2, 0, 75, 50);
    private Rect toolBarArea = new Rect(10, 20, 100, 400);
    private Rect guiTextArea = new Rect(380, 45, 400, 150);
    private Rect referenceArea = new Rect(Screen.width - 200, 20, 200, 200);
    private Rect toggleToolbarArea = new Rect(380, 20, 365, 300);

    string CommandReference =
        "Robot Commands: \n Move Forward: DroidOS:moveForward() \n Turn left: DroidOS:turnLeft() \n Turn Right: DroidOS:turnRight() \n Door Commands: \n Open: DroidOS:open() \n Close: DroidOS:close()";
    private Vector2 scrollPosition;
    void OnGUI()
    {
        GUI.Window(4, timeGUIRect, timeFunc, GUIContent.none);
    }

    void timeFunc(int id)
    {
        int time = (int)Time.timeSinceLevelLoad;
        GUIContent timeContent = new GUIContent("Time: " + time.ToString());
        GUILayout.Label(timeContent);

    }

    void guiFunc(int id)
    {
       
                    Droid droidStats;
                    if (selected.GetComponent("Robot") == null)
                    { 
                        droidStats = selected.GetComponent("Door") as Door; 
                    }
                    else
                    { 
                        droidStats = selected.GetComponent("Robot") as Robot;
                    }
                    GUILayout.TextArea(droidStats.timesInteracted.ToString());
    }



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.updateLevelScore(1, GameManager.instance.getLevelScore());
            Application.LoadLevel(0);
        }
    }


}
