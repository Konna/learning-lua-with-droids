﻿using UnityEngine;
using System.Collections;

using NLua;
using System.IO;
using System.Text;
using System;
using System.Collections.Generic;

public class Robot : Droid
{

    public GameObject myTurretObject;
    public GameObject bullet;
    public List<GameObject> locatedObjects = new List<GameObject>();
    public GameObject targetObject;

    private float fireTimer;
    private float fireInterval = 1.5f;
    private float waitTime = 1.0f;
    private float currentWaitTime = 0.0f;
    float turnTime = 0.0f;
    float jumpTime = 0.0f;
    public Quaternion qTo = Quaternion.identity;
    public float rotation = 0.0f;
    Vector3 direction;
    Quaternion lookRotation;


    public GameObject MyCannon
    {
        get { return myTurretObject; }
    }

    void Awake()
    {
        type = "robot";
    }

    public void Start()
    {
    }

    #region Commands

    /// <summary>
    /// This coroutine is the bare bones of the system to translate user lua code into actions.
    /// It functions to manage each corountine and only finish working once each command in the group is finished.
    /// This function is an override of the general driod version so it can run the aim at target corountine properly.
    /// </summary>
    /// <returns></returns>
    public override IEnumerator run()
    {
        timesInteracted += 1;
        while (running)
        {
            if (!acting)
            {
                if (commandGroup[command].StartsWith("aimAtTarget"))
                {
                    string theCommand = commandGroup[command].Substring(0, commandGroup[command].IndexOf(" "));
                    string theTarget = commandGroup[command].Substring(commandGroup[command].IndexOf(" "));
                    StartCoroutine(aimAtTarget(Int32.Parse(theTarget)));
                }
                else if (commandGroup[command].StartsWith("checkTarget"))
                {
                    string theCommand = commandGroup[command].Substring(0, commandGroup[command].IndexOf(" "));
                    string theTarget = commandGroup[command].Substring(commandGroup[command].IndexOf(" "));
                    StartCoroutine(checkTarget(Int32.Parse(theTarget)));
                }
                else
                {
                    StartCoroutine(commandGroup[command]);
                }

            }
            yield return 0;
        }
    }

    /// <summary>
    /// This coroutine forms part of the core system to translate the users lua code into actions.
    /// This coroutine functions to locate the targets in world and store them in the robot for use.
    /// </summary>
    /// <returns></returns>
    IEnumerator findTargets()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        var objectsFound = Physics.OverlapSphere(transform.position, 50.0f);
        for (int i = 0; i < objectsFound.Length; i++)
        {
            if (objectsFound[i].gameObject.name == "Target")
            {
                locatedObjects.Add(objectsFound[i].gameObject);
            }
        }
        yield return 0;
        acting = false;
    }

    IEnumerator checkTarget(int target)
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        if (locatedObjects[target].tag == "Friendly")
        {
            yield return true;
            acting = false;
        }
        else
        {
            yield return false;
            acting = false;
        }
        acting = false;
    }

    /// <summary>
    /// This coroutine forms part of the core system to translate the users lua code into actions.
    /// This coroutine functions to aim at a target from the list of gathered targets.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    IEnumerator aimAtTarget(int target)
    { 
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        targetObject = locatedObjects[target];
        direction = (locatedObjects[target].transform.position - myTurretObject.transform.position).normalized;
        lookRotation = Quaternion.LookRotation(direction);
        while (myTurretObject.transform.rotation != lookRotation)
        {
            turnTime += Time.deltaTime;
            float timer2 = turnTime;
            if (timer2 > 1.0f)
            {
                turnTime = 0.0f;
            }
            myTurretObject.transform.rotation = Quaternion.RotateTowards(myTurretObject.transform.rotation, lookRotation, 0.5f);
            if (myTurretObject.transform.rotation == lookRotation)
            {
                acting = false;
            }
            yield return 0;
        }
        myTurretObject.transform.rotation = lookRotation;
    }

    /// <summary>
    /// This coroutine forms part of the core system to translate the users lua code into actions.
    /// This coroutine functions to fire a single bullet from the robot.
    /// </summary>
    /// <returns></returns>
    IEnumerator fire()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        GameObject newBullet = (GameObject)Instantiate(bullet, myTurretObject.transform.position, myTurretObject.transform.FindChild("TurretCannon").transform.rotation);
        Bullet newScript = newBullet.GetComponent<Bullet>();
        newScript.targetObject = targetObject;
        shotRecently = true;
        fireTimer = Time.time;
        yield return 0;
        acting = false;
    }

    /// <summary>
    /// This coroutine is used to stop collision errors occuring in for loops.
    /// </summary>
    /// <returns></returns>
    IEnumerator waitForCompletion()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }

        yield return new WaitForSeconds(1.0f);
        acting = false;

    }
    /// <summary>
    /// This method is used to move the object towards the currently targeted object, it is controlled by the DroidOS class.
    /// </summary>
    /// <param name="targetedObject"></param>
    void goToTarget(GameObject targetedObject)
    {
        //TODO: debug this method, currently the robot is going onto the object it is targetting.
        targetObject = targetedObject;
        float maxMovement = 500.0f;
        float moveDistance = maxMovement * Time.deltaTime;
        Vector3 source = transform.position;
        Vector3 target = targetedObject.transform.position;

        Vector3 seekVelocity = seek(source, target, moveDistance);
        print(seekVelocity);
        //transform.position = Vector3.MoveTowards(source, target, moveDistance);
        GetComponent<Rigidbody>().AddForce(seekVelocity, ForceMode.VelocityChange);
    }

    /// <summary>
    /// This coroutine allows the robot to turn left outside of the update. It is flagged to run from the command group list
    /// it forms part of the core system for translating the users lua input into actions the robot takes.
    /// </summary>
    /// <returns></returns>
    IEnumerator turnLeft()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        rotation -= 90.0f;
        qTo = Quaternion.Euler(0.0f, rotation, 0.0f);
        while (transform.rotation != qTo)
        {
            turnTime += Time.deltaTime;
            float timer2 = turnTime;
            if (timer2 > 1.0f)
            {
                turnTime = 0.0f;
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, qTo, 0.5f * timer2);
            if (transform.rotation == qTo)
            {
                acting = false;
            }
            yield return 0;
        }
    }

    /// <summary>
    /// This coroutine forms part of the core system for translating the users lua code into actions.
    /// This coroutine functions to turn the robot 90 degrees to the right.
    /// </summary>
    /// <returns></returns>
    IEnumerator turnRight()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        rotation += 90.0f;
        qTo = Quaternion.Euler(0.0f, rotation, 0.0f);
        while (transform.rotation != qTo)
        {
            turnTime += Time.deltaTime;
            float timer2 = turnTime;
            if (timer2 > 1.0f)
            {
                turnTime = 0.0f;
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, qTo, 0.5f * timer2);
            if (transform.rotation == qTo)
            {
                acting = false;
            }
            yield return 0;
        }
    }

    /// <summary>
    /// This coroutine forms part of the core system for translating the lua code into actions.
    /// This coroutine functions to move the robot forward a set distance, it moves in the direction that the robot is facing.
    /// </summary>
    /// <returns></returns>
    IEnumerator moveForward()
    {
        acting = true;
        if (command < commandGroup.Count - 1)
        {
            command++;
        }
        else if (command == commandGroup.Count - 1)
        {
            running = false;
        }
        float rate = 1.0f;
        float currentRate = 0.0f;
        Vector3 currentLocation = transform.position;
        Vector3 endLocation = currentLocation + transform.forward;
        while (currentRate < 1.0f)
        {
            transform.position = Vector3.Lerp(currentLocation, endLocation, currentRate);
            currentRate += (rate * Time.deltaTime);
            if (transform.position == endLocation)
            {
                acting = false;
            }
            yield return 0;
        }
        transform.position = endLocation;
        acting = false;
    }

    private Vector3 seek(Vector3 source, Vector3 target, float moveDistance)
    {
        Vector3 direction = Vector3.Normalize(target - source);
        Vector3 velocityToTarget = moveDistance * direction;
        transform.LookAt(targetObject.transform);
        return velocityToTarget - GetComponent<Rigidbody>().velocity;
    }
    #endregion

    /// <summary>
    /// The update function runs once every frame, it is used to update when the user can shoot.
    /// </summary>
    void Update()
    {
        if (shotRecently == true)
        {
            if (Time.time - fireTimer >= fireInterval) shotRecently = false;
        }
    }

}
