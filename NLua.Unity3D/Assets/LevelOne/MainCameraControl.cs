﻿using UnityEngine;
using System.Collections;

public class MainCameraControl : MonoBehaviour
{
    float scrollSpeed = 0.05f;
    float dragSpeed = 0.5f;
    float rotateSpeed = 0.5f;


    void Awake()
    {

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePos = Input.mousePosition;
        //By mousePos note: Don't want to move when os is open
        if (DroidOS.instance.isVisible == false)
        {
            if (Input.mousePosition.x > Screen.width - 50)
            {
                transform.Translate(Vector3.right * scrollSpeed);
            }

            if (Input.mousePosition.x < 0 + 50)
            {
                transform.Translate(Vector3.left * scrollSpeed);
            }

            if (Input.mousePosition.y > Screen.height - 50)
            {
                transform.Translate(Vector3.up * scrollSpeed);
            }

            if (Input.mousePosition.y < 0 + 50)
            {
                transform.Translate(Vector3.down * scrollSpeed); ;
            }
        }

        //Rotating
        if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftControl))
        {
            float xRotation = Input.GetAxis("Mouse Y") * rotateSpeed;
            float yRotation = Input.GetAxis("Mouse X") * rotateSpeed;
            xRotation += transform.rotation.eulerAngles.x;

            yRotation += transform.rotation.eulerAngles.y;

            if (yRotation < 180 && yRotation >= 0)
            {
                yRotation = Mathf.Clamp(yRotation, 0, 10);
            }
            else if ((yRotation < -0.1 && yRotation != 0) || yRotation > 180)
            {
                yRotation = Mathf.Clamp(yRotation, 350, 360);
            }
            xRotation = Mathf.Clamp(xRotation, 15.0f, 40.0f);

            transform.rotation = Quaternion.Euler(new Vector3(xRotation, yRotation, 0));
        }

        //By keyboard
        transform.Translate(new Vector3(Input.GetAxis("Horizontal") * scrollSpeed, Input.GetAxis("Vertical") * scrollSpeed, 0));


        //Middle mouse button
        if (Input.GetMouseButton(2)) { transform.Translate(-new Vector3(Input.GetAxis("Mouse X") * dragSpeed, Input.GetAxis("Mouse Y") * dragSpeed, 0)); }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize + 1;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize - 1;
        }

        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 5, 13);
        Vector3 clampTrans = transform.position;

        clampTrans.x = Mathf.Clamp(clampTrans.x, -12, 2);
        clampTrans.y = Mathf.Clamp(clampTrans.y, 6, 16);
        clampTrans.z = Mathf.Clamp(clampTrans.z, -1, 7);
        transform.position = clampTrans;

    }

}
