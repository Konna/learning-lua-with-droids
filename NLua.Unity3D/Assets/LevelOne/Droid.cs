﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class Droid : MonoBehaviour
{
    public List<string> commandGroup = new List<string>();
    protected int command = 0;
    public bool running = false;
    public bool acting = true;
    protected string source = @"";
    float speed = 0.5f;
    protected string type = "droid";
    protected bool shotRecently = true;
    public int timesInteracted = 0;

    public string Type
    {
        get { return type; }
    }
    public float Speed
    {
        get { return speed; }
    }

    public string Source
    {
        get { return source; }
    }

    public bool CanFire
    {
        get
        {
            if (type.Equals("droid")) return false;
            if (type.Equals("door")) return false;
            if (shotRecently == true) return false;
            return true;
        }
    }

    public void clearCommands()
    {
        command = 0;
        commandGroup.Clear();
    }

    public void startRunSystem()
    {
        StartCoroutine("run");
    }

    /// <summary>
    /// A coroutine designed to manage the other action coroutines, this stops the coroutines from running at the same time
    /// a key issue with other implementations. this coroutine can be used to check when the users code has been completed 
    /// and therefore can be used to complete (success or failure) of a level.
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerator run()
    {
        timesInteracted += 1;
        while (running)
        {
            if (!acting)
            {
                StartCoroutine(commandGroup[command]);
            }
            yield return 0;
        }
    }
}

