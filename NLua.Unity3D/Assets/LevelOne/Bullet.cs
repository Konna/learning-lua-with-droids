﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// The speed the bullet moves 
   float speed  = 0.4f; 
   
   // The number of seconds before the bullet is automatically destroyed 
   float SecondsUntilDestroy = 10; 
   
   private float startTime;
   public GameObject targetObject;
   void Start () 
   {
       startTime = Time.time; 
   } 
   
   void FixedUpdate ()
   {
       transform.position = Vector3.MoveTowards(transform.position, targetObject.transform.position, speed);
       
       // If the Bullet has existed as long as SecondsUntilDestroy, destroy it 
       if (Time.time - startTime >= SecondsUntilDestroy) {
           Destroy(this.gameObject);
       } 
   }
        
   void OnCollisionEnter(Collision collision)
   {
       
       // Remove the Bullet from the world 
       Destroy(this.gameObject); 
   }
}
