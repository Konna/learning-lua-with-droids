﻿using UnityEngine;
using System.Collections;

public class Goals : MonoBehaviour
{
    Rect goalGUIRect = new Rect(Screen.width - 200, Screen.height / 2 - 150, 200, 200);
    Rect goalGUIReset = new Rect(Screen.width - 200, Screen.height / 2 - 150 , 200, 200);
    Rect goalToggleButtion = new Rect(Screen.width - 200, Screen.height / 2 - 180, 200, 30);
    bool isVisible = true;
    bool acting = false;
    // Use this for initialization
    void Start()
    {

    }

    IEnumerator openGoals()
    {
        if (isVisible || acting)
        {
            yield return 0;
        }
        print("open Coroutine");
        isVisible = true;
        acting = true;
        float rate = 1.0f;
        float currentRate = 0.0f;
        Vector2 currentPos = goalGUIRect.position;
        Vector2 endPos = goalGUIReset.position;
        while (currentRate < 1.0f)
        {
            goalGUIRect.position = Vector2.Lerp(currentPos, endPos, currentRate);
            currentRate += (rate * Time.deltaTime);
            if (currentRate >= 1.0f)
            {
                acting = false;
            }
            yield return 0;
           
        }
        currentPos = endPos; 
    }

    IEnumerator closeGoals()
    {
        if (!isVisible || acting)
        {
            yield return 0;
        }
        print("close Coroutine");
        acting = true;
        float rate = 1.0f;
        float currentRate = 0.0f;
        Vector2 currentPos = goalGUIRect.position;
        Vector2 endPos = new Vector2(goalGUIRect.xMax + 1, goalGUIRect.yMin);
        while (currentRate < 1.0f)
        {
            goalGUIRect.position = Vector2.Lerp(currentPos, endPos, currentRate);
            currentRate += (rate * Time.deltaTime);
            if (currentRate >= 1.0f)
            {
                print("stuff happening");
                acting = false;
                isVisible = false;
            }
            yield return 0;
           
        }
        goalGUIRect.position = endPos;
    }

    void OnGUI()
    {
        //GUIContent goalButton = new GUIContent("Toggle Goals");
        //GUI.Button(goalToggleButtion, goalButton);
        //if (GUI.changed)
        //{
        //    print("noticed click");
        //    if (isVisible)
        //    {
        //        if (!acting)
        //        {
        //            StartCoroutine(closeGoals());
        //        }
        //    }
        //    else
        //    {
        //        if (!acting)
        //        {
        //            StartCoroutine(openGoals());
        //        }
        //    }
        //}
        //if (isVisible)
        //{
        //    GUI.Window(2, goalGUIRect, guiFunc, "goals");
        //}
    }

    void guiFunc(int id)
    {
        GUILayout.TextArea("stuff");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
