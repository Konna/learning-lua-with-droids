﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
class Door : Droid
{
    bool isOpen = false;
    public GameObject slidingDoor;

    void Awake()
    {
        type = "door";
    }

    #region Commands
    IEnumerator close()
    {
        if (isOpen)
        {
            acting = true;
            if (command < commandGroup.Count - 1)
            {
                command++;
            }
            else if (command == commandGroup.Count - 1)
            {
                running = false;
            }
            float rate = 1.0f;
            float currentRate = 0.0f;
            Vector3 currentLocation = slidingDoor.transform.position;
            Vector3 endLocation = currentLocation + new Vector3(0, 2, 0);
            while (currentRate < 1.0f)
            {
                slidingDoor.transform.position = Vector3.Lerp(currentLocation, endLocation, currentRate);
                currentRate += (rate * Time.deltaTime);
                if (slidingDoor.transform.position == endLocation)
                {
                    isOpen = true;
                    acting = false;
                }
                yield return 0;
            }
            slidingDoor.transform.position = endLocation;
            acting = false;
        }
        else
        {
            yield return 0;
        }
    }

    IEnumerator open()
    {
        if (!isOpen)
        {
            acting = true;
            if (command < commandGroup.Count - 1)
            {
                command++;
            }
            else if (command == commandGroup.Count - 1)
            {
                running = false;
            }
            float rate = 1.0f;
            float currentRate = 0.0f;
            Vector3 currentLocation = slidingDoor.transform.position;
            Vector3 endLocation = currentLocation - new Vector3(0, 2, 0);
            while (currentRate < 1.0f)
            {
                slidingDoor.transform.position = Vector3.Lerp(currentLocation, endLocation, currentRate);
                currentRate += (rate * Time.deltaTime);
                if (slidingDoor.transform.position == endLocation)
                {
                    isOpen = true;
                    acting = false;
                }
                yield return 0;
            }
            slidingDoor.transform.position = endLocation;
            acting = false;
        }
        else
        {
            yield return 0;
        }
    }
    #endregion
}
