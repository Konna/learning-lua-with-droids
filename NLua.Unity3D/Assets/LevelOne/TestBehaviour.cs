﻿using UnityEngine;
using System.Collections;

using NLua;
using System.IO;
using System;
using System.Text;

public class TestBehaviour : MonoBehaviour {

	string source = "";
	Lua env;
	private string loadFile(string fileName)
	{
		// Handle any problems that might arise when reading the text
		string line = "";
		try
		{
			// Create a new StreamReader, tell it which file to read and what encoding the file
			// was saved as
			StreamReader theReader = new StreamReader(fileName, Encoding.Default);
			
			// Immediately clean up the reader after this block of code is done.
			// You generally use the "using" statement for potentially memory-intensive objects
			// instead of relying on garbage collection.
			// (Do not confuse this with the using directive for namespace at the 
			// beginning of a class!)
			using (theReader)
			{
				// While there's lines left in the text file, do this:
					line = @theReader.ReadToEnd();
				
				// Done reading, close the reader and return true to broadcast success    
				theReader.Close();
				return line;
			}
		}
		
		// If anything broke in the try block, we throw an exception with information
		// on what didn't work
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			return line;
		}
	}


	void Awake() {
		env = new Lua();
		env.LoadCLRPackage();
		source += loadFile(@"C:\Users\Konna\Downloads\NLua.Unity3D\Assets\Example\luatest.txt");
		env["this"] = this; // Give the script access to the gameobject.
		env["transform"] = transform;

		//System.Object[] result = new System.Object[0];
		try {

			//result = env.DoString(source);
			env.DoString(source);
		} catch(NLua.Exceptions.LuaException e) {
			Debug.LogError(FormatException(e), gameObject);
		}

	}

	void Start() {
		Call("Start");
	}

	void Update() {
		Call("Update");
		if (Input.GetKeyDown (KeyCode.F))
						Call ("Blank");
		if (Input.GetKeyDown (KeyCode.L))
						Call ("Visible");
	}

	void OnGUI() {
		Call("OnGUI");
	}

	public System.Object[] Call(string function, params System.Object[] args) {
		System.Object[] result = new System.Object[0];
		if(env == null) return result;
		LuaFunction lf = env.GetFunction(function);
		if(lf == null) return result;
		try {
			// Note: calling a function that does not
			// exist does not throw an exception.
			if(args != null) {
				result = lf.Call(args);
			} else {
				result = lf.Call();
			}
		} catch(NLua.Exceptions.LuaException e) {
			Debug.LogError(FormatException(e), gameObject);
		}
		return result;
	}

	public System.Object[] Call(string function) {
		return Call(function, null);
	}

	public static string FormatException(NLua.Exceptions.LuaException e) {
		string source = (string.IsNullOrEmpty(e.Source)) ? "<no source>" : e.Source.Substring(0, e.Source.Length - 2);
		return string.Format("{0}\nLua (at {2})", e.Message, string.Empty, source);
	}
}
