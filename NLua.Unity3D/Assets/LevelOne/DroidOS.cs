﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System;
using NLua;
using UnityEngine.UI;

public class DroidOS : MonoBehaviour
{


    Lua env;

    int toolbarNum = 0;
    Vector2 scrollPosition;

    string insertedCode = @"";
    string currentSource;

    Droid selectedDroid;

    public static DroidOS instance;

    public InputField nameInputField = null;


    void Awake()
    {
        env = new Lua();
        env.LoadCLRPackage();
        env["DroidOS"] = DroidOS.instance;
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            InputField.SubmitEvent submitEvent = new InputField.SubmitEvent();
            submitEvent.AddListener(addCode);
            nameInputField.onEndEdit = submitEvent;
            instance = this;
        }
        else if (instance != null && instance != this) Destroy(gameObject);
        gameObject.SetActive(false);


    }

    public void init(Droid setDroid)
    {
        selectedDroid = setDroid;
        currentSource = selectedDroid.Source;
        insertedCode = currentSource;
    }

    /// <summary>
    /// This method restarts the lua side of the robot an
    /// </summary>
    public void clearEnv()
    {
        env.Close();
        env = new Lua();
        env.LoadCLRPackage();
        env["DroidOS"] = DroidOS.instance;
    }

    public bool isVisible
    {
        get { return gameObject.activeSelf; }
    }

    public void toggleActive()
    {
        if (gameObject.activeSelf == true)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
            return;
        }
    }

    #region Robot Commands


    public void open()
    {
        if (selectedDroid.Type.Equals("door"))
        {
            selectedDroid.commandGroup.Add("open");
        }
    }

    public void close()
    {
        if (selectedDroid.Type.Equals("door"))
        {
            selectedDroid.commandGroup.Add("close");
        }
    }

    /// <summary>
    /// This method makes the selected droid find tagets on the map.
    /// </summary>
    public void findTargets()
    {
        selectedDroid.commandGroup.Add("findTargets");
    }

    /// <summary>
    /// This method aims the cannon at the selected target 
    /// </summary>
    public void aimAtTarget(int targetObject)
    {
        if (selectedDroid.Type.Equals("robot"))
        {
            selectedDroid.commandGroup.Add("aimAtTarget " + targetObject);
            selectedDroid.commandGroup.Add("waitForCompletion");
        }
    }

    public void checkTarget(int targetObject)
    {
        if (selectedDroid.Type.Equals("robot"))
        {
            selectedDroid.commandGroup.Add("checkTarget " + targetObject);
        }
    }
    /// <summary>
    /// This method moves the currently selected droid forward
    /// </summary>
    public void moveForward()
    {
        selectedDroid.commandGroup.Add("moveForward");
        selectedDroid.commandGroup.Add("waitForCompletion");
    }

    public void turnRight()
    {
        selectedDroid.commandGroup.Add("turnRight");
    }

    /// <summary>
    /// This method moves the currently selected droid to the left.
    /// </summary>
    public void turnLeft()
    {
        selectedDroid.commandGroup.Add("turnLeft");

    }

    public void fire()
    {
        if (selectedDroid.CanFire == true)
        {
            selectedDroid.commandGroup.Add("fire");
        }
    }

    /// <summary>
    /// This method searches the local area around the robot for any objects then passes these objects to a list stored in the robot.
    /// </summary>
    public void locateObjects()
    {
        //TODO: the layer mask needs to be adjusted to exclude the camera.
        int layer = LayerMask.NameToLayer("Robots");
        int layerMask = ~(1 << layer); //Invert our layermask for the robots 
        var objectsFound = Physics.OverlapSphere(selectedDroid.transform.position, 50.0f, layerMask);
        for (int i = 0; i < objectsFound.Length; i++)
        {
            print("FoundObject");
            //selectedDroid.LocatedObjects.Add(objectsFound[i].gameObject);
        }
    }

    /// <summary>
    /// This method uses the list generated in the search function as well as in index passed in to go towards a target.
    /// </summary>
    /// <param name="index"></param>
    public void goToTarget(int index)
    {
        if (index < 0) return;
        // if (selectedDroid.LocatedObjects == null) return;
        // if (index > selectedDroid.LocatedObjects.Count - 1 ) return;
        // selectedDroid.goToTarget(selectedDroid.LocatedObjects[index]);
    }
    #endregion

    #region Button methods
    public void reload()
    {
        insertedCode = currentSource;
    }

    public void addCode(string code)
    {
        insertedCode = code;
        compileCode();
    }

    public void compileCode()
    {
        clearEnv();
        selectedDroid.clearCommands();
        try
        {
            env.DoString(insertedCode);
            selectedDroid.acting = false;
            selectedDroid.running = true;
            selectedDroid.startRunSystem();
        }
        catch (NLua.Exceptions.LuaException e)
        {
            //TODO: debugging

            // "non instance method" -- user is using "." instead of ":"
            // "unexpected symbol" -- user is using "{ }" instead of "end"
            // "attempt to index global 'droidos' (a nil value)" -- user isn't using DroidOS to perform commands
            Debug.LogError(FormatException(e), gameObject);
        }
        print(insertedCode);

    }

    /// <summary>
    /// Saves the currently displayed text to the file path passed in.
    /// </summary>
    /// <param name="fileName">The filepath to save to. </param>
    /// <returns></returns>
    private bool save(string fileName)
    {
        StreamWriter writer = new StreamWriter(fileName);
        writer.Write(insertedCode);
        return true;
    }


    //TODO: have a function allow the user to load from a selected file which just passes the string of the filepath into this function.

    /// <summary>
    /// Loads a file from a string into the editor.
    /// </summary>
    /// <param name="fileName"> The filepath to load from. </param>
    /// <returns> true unless the file hasn't loaded i.e. doesn't exsist. </returns>
    private bool load(string fileName)
    {
        try
        {
            string line;
            string replacementSource = "";
            StreamReader theReader = new StreamReader(fileName, Encoding.Default);

            using (theReader)
            {
                do
                {
                    line = theReader.ReadLine();

                    if (line != null)
                    {
                        replacementSource += line;
                        replacementSource += "\n";
                    }
                }
                while (line != null);

                insertedCode = replacementSource;

                theReader.Close();
                return true;
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return false;
        }
    }
    #endregion


    /// <summary>
    /// This function runs code from the lua input.
    /// </summary>
    /// <param name="function"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public System.Object[] Call(string function, params System.Object[] args)
    {
        System.Object[] result = new System.Object[0];
        if (env == null) return result;
        LuaFunction lf = env.GetFunction(function);
        if (lf == null) return result;
        try
        {
            // Note: calling a function that does not 
            // exist does not throw an exception.
            if (args != null)
            {
                result = lf.Call(args);
            }
            else
            {
                result = lf.Call();
            }
        }
        catch (NLua.Exceptions.LuaException e)
        {
            Debug.LogError(FormatException(e), gameObject);
        }
        return result;
    }

    public System.Object[] Call(string function)
    {
        return Call(function, null);
    }

    public static string FormatException(NLua.Exceptions.LuaException e)
    {
        string source = (string.IsNullOrEmpty(e.Source)) ? "<no source>" : e.Source.Substring(0, e.Source.Length - 2);
        return string.Format("{0}\nLua (at {2})", e.Message, string.Empty, source);
    }
}
