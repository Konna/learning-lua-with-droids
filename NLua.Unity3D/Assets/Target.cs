﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
    
    public GameObject managerObject;
    TargetManager manager;
	// Use this for initialization
	void Start () {
        manager = managerObject.GetComponent<TargetManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {
        manager.targetList.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
