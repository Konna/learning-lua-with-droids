﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreSheet : MonoBehaviour {

    public Text levelOneTop;
    public Text levelOneSecond;
    public Text levelOneLast;

    public Text levelTwoTop;
    public Text levelTwoSecond;
    public Text levelTwoLast;

    public Text levelThreeTop;
    public Text levelThreeSecond;
    public Text levelThreeLast;



	// Use this for initialization
	void Start () {
        levelOneTop.text =  PlayerPrefs.GetInt("levelOne 1",0).ToString();
        levelOneSecond.text = PlayerPrefs.GetInt("levelOne 2", 0).ToString();
        levelOneLast.text = PlayerPrefs.GetInt("levelOne 3", 0).ToString();

        levelTwoTop.text = PlayerPrefs.GetInt("levelTwo 1", 0).ToString();
        levelTwoSecond.text = PlayerPrefs.GetInt("levelTwo 2", 0).ToString();
        levelTwoLast.text = PlayerPrefs.GetInt("levelTwo 3", 0).ToString();

        levelThreeTop.text = PlayerPrefs.GetInt("levelThree 1", 0).ToString();
        levelThreeSecond.text = PlayerPrefs.GetInt("levelThree 2", 0).ToString();
        levelThreeLast.text = PlayerPrefs.GetInt("levelThree 3", 0).ToString();
	}
	
    public void menuOnClick()
    {
        Application.LoadLevel(0);
    }

    public void resetScoreSheet()
    {
        PlayerPrefs.SetInt("levelOne 1", 0);
        PlayerPrefs.SetInt("levelOne 2", 0);
        PlayerPrefs.SetInt("levelOne 3", 0);
        PlayerPrefs.SetInt("levelTwo 1", 0);
        PlayerPrefs.SetInt("levelTwo 2", 0);
        PlayerPrefs.SetInt("levelTwo 3", 0);
        PlayerPrefs.SetInt("levelThree 1", 0);
        PlayerPrefs.SetInt("levelThree 2", 0);
        PlayerPrefs.SetInt("levelThree 3", 0);
    }

	// Update is called once per frame
	void Update () {
        levelOneTop.text = PlayerPrefs.GetInt("levelOne 1", 0).ToString();
        levelOneSecond.text = PlayerPrefs.GetInt("levelOne 2", 0).ToString();
        levelOneLast.text = PlayerPrefs.GetInt("levelOne 3", 0).ToString();

        levelTwoTop.text = PlayerPrefs.GetInt("levelTwo 1", 0).ToString();
        levelTwoSecond.text = PlayerPrefs.GetInt("levelTwo 2", 0).ToString();
        levelTwoLast.text = PlayerPrefs.GetInt("levelTwo 3", 0).ToString();

        levelThreeTop.text = PlayerPrefs.GetInt("levelThree 1", 0).ToString();
        levelThreeSecond.text = PlayerPrefs.GetInt("levelThree 2", 0).ToString();
        levelThreeLast.text = PlayerPrefs.GetInt("levelThree 3", 0).ToString();

	}
}
