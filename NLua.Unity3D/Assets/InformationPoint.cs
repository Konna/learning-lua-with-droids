﻿using UnityEngine;
using System.Collections;

public class InformationPoint : MonoBehaviour {

    public TextAsset myTutorial;
    public GameObject canvas;
    public bool markedAsFinal = false;
    UIManager UI;
	// Use this for initialization
	void Start ()
    {
        if (canvas != null)
        {
            UI = canvas.GetComponent<UIManager>();
        }
	}
	
	// Update is called once per frame
	void Update () {

        var hitColliders = Physics.OverlapSphere(gameObject.transform.position, 4.0f);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject.name == "Robot")
            {
                if (myTutorial != null)
                {
                    UI.guideText.text = myTutorial.text;
                    Canvas.ForceUpdateCanvases();
                }
                if (markedAsFinal)
                {
                    GameManager.instance.updateLevelScore(1, GameManager.instance.getLevelScore());
                    if (DroidOS.instance.gameObject.activeSelf)
                    {
                        DroidOS.instance.toggleActive();
                    }
                    Application.LoadLevel(0);
                }
            }
        }
	}
}
