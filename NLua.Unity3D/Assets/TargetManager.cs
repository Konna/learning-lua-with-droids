﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager : MonoBehaviour {

    public List<GameObject> targetList = new List<GameObject>();
	void Start () {
        targetList.Clear();
        var objectsFound = Physics.OverlapSphere(transform.position, 50.0f);
        for (int i = 0; i < objectsFound.Length; i++)
        {
            if (objectsFound[i].gameObject.name == "Target")
            {
                targetList.Add(objectsFound[i].gameObject);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	if (targetList.Count == 0)
    {
        GameManager.instance.updateLevelScore(2, GameManager.instance.getLevelScore());
        if (DroidOS.instance.gameObject.activeSelf)
        {
            DroidOS.instance.toggleActive();
        }
        Application.LoadLevel(0);

    }
	}
}
