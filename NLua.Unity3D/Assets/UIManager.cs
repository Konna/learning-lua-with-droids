﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    RaycastHit hit;
    public static GameObject selectedDroid;
    public GameObject UIPanel;
    public GameObject buttonPrefab;
    public Text toggleButtonText;
    public Text guideText;
    public Text statsText;
    public GameObject[] mapDroids;
    [HideInInspector]
    public Droid targetRobot;
    private Vector3 mouseDownAt;
    private Rect mainGUIRect = new Rect(Screen.width / Screen.width, Screen.height - 200, Screen.width, 200);

    // Use this for initialization
    void Awake()
    {
        mouseDownAt = Vector3.zero;
    }
    void OnLevelWasLoaded(int level)
    {
        GameManager.instance.getRobotsOnLevelLoad();
        mapDroids = new GameObject[GameManager.instance.robots.Length];
        print(GameManager.instance.robots.Length);
    }

    void buttonScriptGen(Droid robot)
    {
        if (DroidOS.instance.gameObject.activeSelf == true)
        {
            DroidOS.instance.toggleActive();
        }
        GameObject selected = robot.GetComponentInParent<Transform>().gameObject;
        if (selected.transform.FindChild("Selected"))
        {
            if (selectedDroid != selected.gameObject)
            {
                Debug.Log("Found unit");
                GameObject selectedObject = selected.transform.FindChild("Selected").gameObject;
                selectedObject.SetActive(true);
                if (selectedDroid != null)
                {
                    selectedDroid.transform.FindChild("Selected").gameObject.SetActive(false);
                }
                selectedDroid = selected.gameObject;
            }

        }
        else
        {
            selectedDroid.transform.FindChild("Selected").gameObject.SetActive(false);
            selectedDroid = null;
        }
        DroidOS.instance.init(robot);
        DroidOS.instance.toggleActive();
    }

    public void toggleInterface()
    {
        if (selectedDroid.GetComponent("Robot") == null)
        { targetRobot = selectedDroid.GetComponent("Door") as Door; }
        else { targetRobot = selectedDroid.GetComponent("Robot") as Robot; }
        DroidOS.instance.init(targetRobot);
        DroidOS.instance.toggleActive();
    }

    public void goalsButton()
    {
        if (guideText.gameObject.activeSelf == false)
        {
            if (statsText.gameObject.activeSelf == true) statsText.gameObject.SetActive(false);
            guideText.gameObject.SetActive(true);
        }
    }

    public void statsButton()
    {
        if (statsText.gameObject.activeSelf == false)
        {
            if (guideText.gameObject.activeSelf == true) guideText.gameObject.SetActive(false);
            statsText.gameObject.SetActive(true);
        }
        Droid droidStats;
        if (selectedDroid.GetComponent("Robot") == null)
        {
            droidStats = selectedDroid.GetComponent("Door") as Door;
        }
        else
        {
            droidStats = selectedDroid.GetComponent("Robot") as Robot;
        }
        statsText.text = droidStats.timesInteracted.ToString();
    }

    void Start()
    {
        RectTransform containerTransform = UIPanel.GetComponent<RectTransform>();
        print(containerTransform.rect);
        for (int i = 0; i < GameManager.instance.robots.Length; i++)
        {
            Droid robot;
            string newText;
            if (GameManager.instance.robots[i].GetComponent("Robot") == null)
            {
                robot = GameManager.instance.robots[i].GetComponent("Door") as Door;
                newText = robot.Type;
            }
            else
            {
                robot = GameManager.instance.robots[i].GetComponent("Robot") as Robot;
                newText = robot.Type;
            }
            mapDroids[i] = Instantiate(buttonPrefab) as GameObject;
            mapDroids[i].transform.SetParent(UIPanel.gameObject.transform);
            mapDroids[i].GetComponentInChildren<Text>().text = newText;
            Button script = mapDroids[i].GetComponent<Button>() as Button;
            RectTransform rectTransform = mapDroids[i].GetComponent<RectTransform>();
            script.onClick.AddListener(() => buttonScriptGen(robot));
            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10f, 160.0f);
            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10f + (i * 100.0f), 30.0f);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DroidOS.instance.gameObject.activeSelf == true)
        {
            toggleButtonText.text = "Close Editor";
        }
        else
        {
            toggleButtonText.text = "Open Editor";
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseDownAt = hit.point;
            }
            if (hit.collider.name == "Terrain")
            {
                if (Input.GetMouseButtonUp(0) && didUserClickLeftMouse(mouseDownAt) && (DroidOS.instance.isVisible != true))
                {
                    Debug.Log("found ground");
                    deselectSelected();
                }

            }
            else
            {
                if (Input.GetMouseButtonUp(0) && didUserClickLeftMouse(mouseDownAt))
                {
                    if (hit.collider.transform.FindChild("Selected"))
                    {
                        if (selectedDroid != hit.collider.gameObject)
                        {
                            Debug.Log("Found unit");
                            GameObject selectedObject = hit.collider.transform.FindChild("Selected").gameObject;
                            selectedObject.SetActive(true);
                            if (selectedDroid != null)
                            {
                                selectedDroid.transform.FindChild("Selected").gameObject.SetActive(false);
                            }
                            selectedDroid = hit.collider.gameObject;
                        }

                    }
                    else
                    {
                      deselectSelected();
                    }
                }

            }
        }
    }
    public bool didUserClickLeftMouse(Vector3 hitPoint)
    {
        float clickZone = 3.5f;
        Vector2 mousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        Vector3[] bufferArea = new Vector3[2];
        bufferArea[0] = new Vector3(hitPoint.x + clickZone, hitPoint.y + clickZone, hitPoint.z + clickZone);
        bufferArea[1] = new Vector3(hitPoint.x - clickZone, hitPoint.y - clickZone, hitPoint.z - clickZone);
        print(mousePos);
        if (mainGUIRect.Contains(mousePos, true)) { return false; }
        if (mouseDownAt.x < bufferArea[0].x && mouseDownAt.x > bufferArea[1].x &&
            mouseDownAt.y < bufferArea[0].y && mouseDownAt.y > bufferArea[1].y &&
            mouseDownAt.z < bufferArea[0].z && mouseDownAt.z > bufferArea[1].z
           ) return true;
        return false;
    }

    public static void deselectSelected()
    {
        if (selectedDroid != null)
        {
            selectedDroid.transform.FindChild("Selected").gameObject.SetActive(false);
            selectedDroid = null;
        }
    }
}
